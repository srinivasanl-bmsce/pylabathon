//Find the largest of three numbers using simple if.
int input()
{int a;
printf("Enter a number");
scanf("%d", &a);
return a;
}

int greatest(int a, int b, int c)
{
	int g=(a>=b && a>=c? a: ((b>=c)? b: c));
	return g;
}

int output(int a)
{
printf("The greatest number is %d", a);
}

int main()
{   int a, b, c, g;
	a=input(); 
	b=input();
	c=input();
	g=greatest(a, b, c);

	output(g);
	return 0;
}
