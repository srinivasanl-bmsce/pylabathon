//Illustrate pointers in swapping two numbers
#include<math.h>
int input()
{
    int a;   
    printf("enter the number");
    scanf("%d",&a);
    return a;
}
int compute(int *a,int *b)
{
  int temp;
  temp=*a;
   *a=*b;
   *b=temp;
}


int main()
{
 int a,b,c,d,e;
a=input();
b=input();
e=compute(&a,&b);
printf("%d %d",a,b);

}