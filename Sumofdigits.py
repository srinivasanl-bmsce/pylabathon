//Develop a C program to enter a number and calculate the sum of digits. #include<stdio.h>
#include<math.h>
int input()
{
    int a;   
    printf("enter the number\n");
    scanf("%d",&a);
    return a;
}
int compute(int a)
{
    int r,sum=0;
    while(a!=0)
    {
      r=a%10;
     sum=sum+r;
     a=a/10;
    }
    return sum;
}
void output(int e) 
{
   printf("sum of digits=%d",e);
}
int main()
{
 int a,b,c,d,e;
a=input();
e=compute(a);
output(e);
}
